// Visual Studio 2010
#include <fstream>
#include <iostream>

int main (int argc, char **argv) {
  std::ifstream fin("input.txt");
  std::ofstream fout("output.txt");
  
  int N = 0; // there will be the degree of matrix
  bool b = true;
  int x;
  char y;
  while (b) {
    if (fin >> x) {
      N++;
    } else {
      b = false;
    }
  }
  N = sqrt(float(N));

  fin.clear();
  fin.seekg(0, std::ios::beg);
  /* allocating memory*/
  int **matrix = 0;
  matrix = new int*[N];
  for (int i = 0;i < N;i++) {
    matrix[i] = new int[N];
  }
  /* fill with numbers */
  for (int i = 0;i < N;i++) {
    for (int j = 0;j < N;j++) {
      fin >> matrix[i][j];
    }
  }  
  /* count */
  int sum1, // for elements that upper than main diagonal
      sum2, // the same but lower
      cmax, // current maximum
      smax = 0; // maximum among summs

  for (int i = 0;i < N;i++) {
    smax += matrix[i][i];
  }
  for (int i = 1;i < N;i++) {
    sum1 = sum2 = 0;
    for (int j = 0;j < N - i;j++) {

      sum1 += matrix[j][j + i];
      sum2 += matrix[j + i][j];
    }
    cmax = std::max(sum1, sum2);
    if (cmax > smax) {
      smax = cmax;
    }
  }
  
  fout << smax;

  fin.close();
  fout.close();
  return 0;
}