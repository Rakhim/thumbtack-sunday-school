﻿// Visual Studio 2010
#include <fstream>
#include <set>

int main (int argc, char **argv) {
  std::ifstream fin("input.txt");
  std::ofstream fout("output.txt");
  std::set<size_t> set;
  
  size_t i = 0; // for count the number of elements
  size_t x; // auxilary variable ; natural numbers
  size_t max = 0; // maximum

  while (fin) {
    i++;
    fin >> x;
    set.insert(x);
    if (x > max) {
      max = x;
    }
  }

  // I guess the Natural numbers with the zero;
  // for another chose just change (max + 1) to (max)  
  if (set.size() == max + 1) {
    fout << "Да";
  } else {
    fout << "Нет";
  }
    
  fin.close();
  fout.close();

  return 0;
}