// Visual Studio 2010
#include <fstream>
#include <vector>

int main (int argc, char **argv) {

  std::ifstream fin("input.txt");
  std::ofstream fout("output.txt");
  std::vector<char> word;  
  
  int N; // (length / 2) of current encountered word
  char c;
  bool b; // it's the palindrom
  char * lastWord = 0; // last word that pretending being the longest
  size_t lastWordLength = 0; // length of that word
  while(fin) {
    
    b = true; // we guess, yes we will face with a palindrom
    while (((c = fin.peek()) != ' ') && c != EOF) {
      fin >> c;
      word.push_back(c);
    }
    
    // eat all spaces because they are not important for us
    while(fin.peek() == ' ') {
      fin.read(&c, 1);
    }

    //for (int i = 0;i < word.size();i++) std::cout << " " << word[i];
  
    /* test out theory */
    N = size_t(word.size() / 2);
    for (size_t i = 0;i < N && b;i++) {
      if (word[i] != word[word.size() - i - 1]) {
        b = false;
      }
    }
    // ok , we face with a palindrom 
    if (b && (word.size() > lastWordLength)) {
      delete [] lastWord;
      lastWord = new char[word.size()];
      for (size_t i = 0;i < word.size();i++) {
        lastWord[i] = word[i];
      }
      
      lastWordLength = word.size();
    }
    // we will forget that word
    word.clear();
  }
  
  if (lastWordLength) {
    for (int i = 0;i < lastWordLength;i++) {
      fout << lastWord[i];
    }
  } else {
    fout << "отсутствует";
  }

  fin.close();
  fout.close();
  return 0;
}